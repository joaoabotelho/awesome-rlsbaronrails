class CocktailsController < ApplicationController
  before_action :set_cocktail, only: :show

  INCLUDE_ATTRS = [{category: {only: [:id, :name]} },
                   {ingredients: {only: [:id, :name]}},
                   {tags: {only: [:id, :name]}}]
  EXCEPT_ATTRS = [:category_id, :created_at, :updated_at]


  # GET /cocktails
  def index
    @cocktails = fetch_cocktails()
    render json: @cocktails, include: INCLUDE_ATTRS, except: EXCEPT_ATTRS, methods: :measures
  end

  # GET /cocktails/1
  def show
    render json: @cocktail, include: INCLUDE_ATTRS, except: EXCEPT_ATTRS
  end

  # GET /cocktails/random
  def random
    @cocktail = Cocktail.get_random()

    render json: @cocktail, include: INCLUDE_ATTRS, except: EXCEPT_ATTRS, methods: :measures
  end

  private

  def fetch_cocktails
    if cocktail_params[:category_id]
      Cocktail.by_category_id(cocktail_params[:category_id])
    elsif cocktail_params[:name]
      Cocktail.with_name(cocktail_params[:name])
    elsif cocktail_params[:starts_with]
      Cocktail.starts_with(cocktail_params[:starts_with])
    elsif cocktail_params[:ingredient]
      Cocktail.with_ingredient_name(cocktail_params[:ingredient])
      #@cocktails = Cocktail.joins(:ingredients).where(ingredients: {id: [1,2]}).group(:id).having("COUNT(cocktails_ingredients.cocktail_id) = 2")
    else
      Cocktail.all
    end
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_cocktail
    @cocktail = Cocktail.find(params[:id])
  end

  # Only allow a trusted parameter "white list" through.
  def cocktail_params
    params.permit(:name, :ingredient, :starts_with, :category_id)
  end
end
