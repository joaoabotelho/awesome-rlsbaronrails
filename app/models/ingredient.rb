class Ingredient < ApplicationRecord
  validates :name, presence: true, uniqueness: true
  has_many :cocktail_ingredient
  has_many :cocktails, through: :cocktail_ingredient
end
