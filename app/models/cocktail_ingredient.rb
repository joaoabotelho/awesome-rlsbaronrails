class CocktailIngredient < ApplicationRecord
  self.table_name = "cocktails_ingredients"

  belongs_to :cocktail
  belongs_to :ingredient
end
