class Cocktail < ApplicationRecord
  belongs_to :category
  has_many :cocktail_ingredient
  has_many :ingredients, through: :cocktail_ingredient
  has_and_belongs_to_many :tags

  attr_accessor :measures
  default_scope { preload(:cocktail_ingredient).joins(:ingredients, :category).left_outer_joins(:tags).includes(:ingredients, :category, :tags) }

  def self.by_category_id(category_id)
    Category.find_by(id: category_id)&.cocktails || []
  end

  def self.with_name(name)
    where("cocktails.name LIKE ?", "%#{name}%")
  end

  def self.starts_with(start)
    where("cocktails.name LIKE ?", "#{start}%")
  end

  def self.with_ingredient_name(ingredient)
    where("ingredients.name LIKE ?", "%#{ingredient}%")
  end

  def self.get_random
    order("RANDOM()").limit(1).first
  end

  def measures
    @measures ||= self.cocktail_ingredient.map do |cocktail_ingredient|
      {
          ingredient_id: cocktail_ingredient.ingredient_id,
          value: cocktail_ingredient.measure
      }
    end
  end
end
