FactoryBot.define do
  factory :cocktail do
    name {Faker::Name.unique.name}
    ingredients  {build_list :ingredient, 3}
    category
  end

  factory :category do
    name {Faker::Name.unique.name}
  end

  factory :ingredient do
    name {Faker::Name.unique.name}
  end
end