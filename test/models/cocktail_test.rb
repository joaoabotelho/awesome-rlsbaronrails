require 'test_helper'

class CocktailTest < ActiveSupport::TestCase
  test "Cocktail.by_category_id finds cocktail with provided category_id" do
    cocktails = create_list(:cocktail, 3)
    expected_cocktail = cocktails.first

    result_cocktails = Cocktail.by_category_id(expected_cocktail.category_id)

    assert result_cocktails.size == 1
    assert result_cocktails.first.id == expected_cocktail.id
  end

  test "Cocktail.by_category_id returns empty list when category_id isn't found" do
    create_list(:cocktail, 3)

    result_cocktails = Cocktail.by_category_id(-1)

    assert result_cocktails.empty?
  end

  test "Cocktail.with_name finds partial match with provided name" do
    create(:cocktail, name: "Mojito")
    create(:cocktail, name: "Caipirinha")

    result_cocktails = Cocktail.with_name("jit")

    assert result_cocktails.first.name == "Mojito"
    assert result_cocktails.size == 1
  end

  test "Cocktail.with_name finds match with provided name not case sensitive" do
    create(:cocktail, name: "Mojito")
    create(:cocktail, name: "Caipirinha")

    result_cocktails = Cocktail.with_name("mojito")

    assert result_cocktails.first.name == "Mojito"
    assert result_cocktails.size == 1
  end

  test "Cocktail.with_name gives all cocktails with provided empty string" do
    create_list(:cocktail, 3)

    result_cocktails = Cocktail.with_name("")

    assert result_cocktails.size == 3
  end

  # test "the truth" do
  #   assert true
  # end
end
