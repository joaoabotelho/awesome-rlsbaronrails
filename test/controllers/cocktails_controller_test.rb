require 'test_helper'

class CocktailsControllerTest < ActionDispatch::IntegrationTest
  test "gets list of all cocktails" do
    create_list(:cocktail, 3)
    get cocktails_url, as: :json

    assert_response :success

    json_response = JSON.parse(response.body)
    assert json_response.size == 3
  end

  test "gets list of cocktails for a provided category_id" do
    cocktails = create_list(:cocktail, 3)
    expected_cocktail = cocktails.first

    get cocktails_url(category_id: expected_cocktail.id), as: :json

    assert_response :success

    json_response = JSON.parse(response.body)
    assert json_response.size == 1
    assert json_response.first['id'] == expected_cocktail.id
  end
end
