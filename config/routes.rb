Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  get '/cocktails/random', to: 'cocktails#random'

  resources :cocktails, only: [:index, :show]
  resources :categories, only: :index
  resources :ingredients, only: :index
end
