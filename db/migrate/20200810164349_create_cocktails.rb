class CreateCocktails < ActiveRecord::Migration[6.0]
  def change
    create_table :cocktails do |t|
      t.string :name
      t.integer :cdb_id
      t.references :category, null: false, foreign_key: true
      t.string :image_url

      t.timestamps
    end
    add_index :cocktails, :name
  end
end
