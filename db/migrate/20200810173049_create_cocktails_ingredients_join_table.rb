class CreateCocktailsIngredientsJoinTable < ActiveRecord::Migration[6.0]
  def change
    create_join_table :cocktails, :ingredients do |t|
      t.index :cocktail_id
      t.index :ingredient_id
    end
  end
end
