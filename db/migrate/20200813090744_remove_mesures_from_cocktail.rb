class RemoveMesuresFromCocktail < ActiveRecord::Migration[6.0]
  def change
    remove_column :cocktails, :measures, :string
  end
end
