class CreateCocktailsTagsJoinTable < ActiveRecord::Migration[6.0]
  def change
    create_join_table :cocktails, :tags do |t|
      t.index :cocktail_id
      t.index :tag_id
    end
  end
end
