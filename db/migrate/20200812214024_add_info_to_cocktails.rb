class AddInfoToCocktails < ActiveRecord::Migration[6.0]
  def change
    add_column :cocktails, :instructions, :string
    add_column :cocktails, :measures, :string
  end
end
