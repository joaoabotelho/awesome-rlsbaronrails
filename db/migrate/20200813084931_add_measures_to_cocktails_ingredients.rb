class AddMeasuresToCocktailsIngredients < ActiveRecord::Migration[6.0]
  def change
    add_column :cocktails_ingredients, :measure, :string
  end
end
